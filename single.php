<?php get_header(); ?>

<section class="article">
	<div class="wide-container">
		<div class="row">
			<div class="breadcrumbs col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center-sm">
				<?php get_template_part( 'part/breadcrumbs' ) ?>
			</div>
			<?php get_template_part( 'part/list-categories' ) ?>
		</div>
		<?php while (have_posts()) : the_post(); ?>
		<div class="article-block">
			<div class="article-header">
				<span><?php the_title(); ?></span>
			</div>
			<div class="article-body row">
				<div class="article-img-container">
					<div class="article-img">
						<?=get_the_post_thumbnail(); ?>
					</div>
				</div>
				<div class="article-content">
					<p><?= the_content(); ?></p>
				</div>
			</div>
		</div>
		<?php endwhile; ?>
	</div>	
</section>
<section class="posts">
		<?php get_template_part( 'part/favorite-companies' ) ?>
</section>
<section class="mini-blog">
	<?php get_template_part( 'part/favorite-posts' ) ?>
</section>
<?php get_footer(); ?>