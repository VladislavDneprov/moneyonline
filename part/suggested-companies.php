<?php

$args = array(
    'numberposts'     => 6,
    // 'offset'          => 0,
    'orderby'         => 'date',
    'order'           => 'ASC',
    // 'meta_key'        => '',
    // 'meta_value'      => '',
    'post_type'       => 'company',
    // 'post_parent'     => '',
    'post_status'     => 'publish',
);

$companies = get_posts($args);
$current_id = get_queried_object_id();

?>

<?php if ($companies): ?>
    <ul>
        <?php foreach ($companies as $index => $company): ?>
            <li <?php echo ($company->ID == $current_id ? 'class="active"' : ''); ?>>
                <a href="<?= get_permalink($company->ID); ?>" title="<?= $company->post_title; ?>">
                    <?= get_the_post_thumbnail($company->ID); ?>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
    <?php else: ?>
    <p>Список компаний пуст.</p>
<?php endif; ?>
