<?php

$meta_key = $_POST['key'];
$meta_value = $_POST['value'];
$sort = ($_POST['sort'] ? 'meta_value_num' : 'ID');

$args = array(
	'numberposts'     => 5,
	'offset'          => 0,
	'orderby'         => $sort,
	'order'           => 'DESC',
	'meta_key'        => ($meta_key ? $meta_key : ''),
	'meta_value'      => ($meta_value ? $meta_value : ''),
	'post_type'       => 'company',
	'post_parent'     => '',
	'post_status'     => 'publish'
);

$companies = get_posts($args);

?>
<div class="wide-container">

	<?php foreach ($companies as $key => $post): ?>	
	<div class="post-block row <?php echo (get_post_meta($post->ID, 'top', 1) ? 'top' : ''); ?> <?php echo (get_post_meta($post->ID, 'new', 1) ? 'new' : ''); ?> <?php echo (get_post_meta($post->ID, 'sale', 1) ? 'sale' : ''); ?>">
			
		<div class="post-img col-lg-3 col-md-3 col-sm-12 col-xs-12 center-sm">
			<a title="Читать о компании..." href="<?= get_permalink($post->ID); ?>" title=""><?= get_the_post_thumbnail($post->ID); ?></a>
		</div>
		<div class="post-content col-lg-7 col-md-7 col-sm-12 col-xs-12">
			<ul>
				<li>
					<p>Рейтинг</p>
					<p>
					<?php if(get_post_meta($post->ID, 'starss', 1)) : ?>
						<?php for($i=1; $i <= get_post_meta($post->ID, 'starss', 1); $i++): ?>
							<i class="fa fa-star"></i>
						<?php endfor; ?>
						<?php else: ?>
							-
					<?php endif; ?>
					</p>
				</li>
				<li>
					<p>На срок</p>
					<p>
						<?php if(get_post_meta($post->ID, 'time_borrow_start', 1)) : ?>
							<?= get_post_meta($post->ID, 'time_borrow_start', 1); ?> - <?= get_post_meta($post->ID, 'time_borrow_finish', 1); ?> дня
						<?php else: ?>
								-
						<?php endif; ?>
					</p>
				</li>
				<li>
					<p>Сумма</p>
					<p>
						<?php if(get_post_meta($post->ID, 'summ', 1)) : ?>
							<?= get_post_meta($post->ID, 'summ', 1); ?>
						<?php else: ?>
								-
						<?php endif; ?>
					</p>
				</li>
				<li>
					<p>Ставка</p>
					<p>
						<?php if(get_post_meta($post->ID, 'rate', 1)) : ?>
							<?= get_post_meta($post->ID, 'rate', 1); ?>%/день
						<?php else: ?>
								-
						<?php endif; ?>
					</p>
				</li>
				<li>
					<p>Рассмотрение</p>
					<p>
						<?php if(get_post_meta($post->ID, 'time', 1)) : ?>
							<?= get_post_meta($post->ID, 'time', 1); ?> часов
						<?php else: ?>
								-
						<?php endif; ?>
					</p>
				</li>
				<li>
					<p>Получение</p>
					<?php if(get_post_meta($post->ID, 'get_way_1', 1)) : ?><img src="<?= get_template_directory_uri().'/img/shit2.png' ?>"><?php endif; ?>
					<?php if(get_post_meta($post->ID, 'get_way_2', 1)) : ?><img src="<?= get_template_directory_uri().'/img/shit1.png' ?>"><?php endif; ?>
				</li>
			</ul>
			<p class="attraction"><?= $post->post_title; ?></p>			
		</div>
		<div class="post-order col-lg-2 col-md-2 col-sm-12 col-xs-12">
			<a target="blank" href="<?= get_post_meta($post->ID, 'url', 1); ?>" title="Перейти на сайт">
				<p>
					Взять<br>кредит!
				</p>
			</a>
		</div>
	</div>
	<?php endforeach; ?>
</div>