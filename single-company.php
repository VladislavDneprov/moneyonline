<?php get_header(); ?>

    <section class="company">
        <div class="wide-container">
            <div class="row">
                <div class="breadcrumbs col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center-sm">
                    <?php get_template_part('part/breadcrumbs') ?>
                </div>
                <div class="to-companies col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center-sm">
                    <div class="filter-block">
                        <button class="dropdown styled-select" data-toggle="dropdown">ПЕРЕЙТИ К ДРУГИМ КОМПАНИЯМ
                        </button>
                        <?php get_template_part('part/list-other-companies') ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <?php if (have_posts()) while (have_posts()) : the_post(); ?>
                        <div class="article-block ">
                            <div class="article-header">
                                <span><?php the_title(); ?></span>
                                <?php if (get_post_meta(get_the_id(), 'starss', 1)) : ?>
                                    <p class="rating">
                                        <?php for ($i = 1; $i <= get_post_meta(get_the_id(), 'starss', 1); $i++): ?>
                                            <i class="fa fa-star"></i>
                                        <?php endfor; ?>
                                    </p>
                                <?php else: ?>
                                    -
                                <?php endif; ?>
                            </div>
                            <div class="article-body row">
                                <div class="article-img-container">
                                    <div class="article-img ">
                                        <?= get_the_post_thumbnail(get_the_id()); ?>
                                    </div>
                                </div>
                                <?php the_content(); ?>
                            </div>
                            <div class="article-footer">
                                <a href="<?= get_post_meta(get_the_id(), 'url', 1); ?>"><img
                                        src="<?= get_template_directory_uri() . '/img/article-footer-text.png'; ?>"></a>
                            </div>
                        </div>
                        <?php
                    endwhile;
                    ?>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="sidebar">
                        <div class="article-block">
                            <div class="article-header">
                                <span>Рекомендуем компании</span>
                            </div>
                            <div class="article-body row">
                                <?php get_template_part('part/suggested-companies'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?/**  php if (comments_open() || get_comments_number()) :
                        comments_template();
                    endif;
                    */?>
                </div>
            </div>
        </div>
    </section>
    <section class="posts">
        <?php get_template_part('part/favorite-companies') ?>
    </section>
    <section class="mini-blog">
        <?php get_template_part('part/favorite-posts') ?>
    </section>

<?php get_footer(); ?>