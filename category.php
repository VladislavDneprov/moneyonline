<?php get_header(); ?>

<section class="blog">
	<div class="wide-container">
		<div class="row">
			<div class="breadcrumbs col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center-sm">
				<?php get_template_part( 'part/breadcrumbs' ) ?>
			</div>
			<?php get_template_part( 'part/list-categories' ) ?>
		</div>
		<div class="row">
			
			<?php while (have_posts()) : the_post(); ?>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="mini-blog-block">
					<div class="mini-blog-header">
						<span><?php the_title(); ?></span>
					</div>
					<div class="mini-blog-body row">
						<div class="mini-blog-img col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<?php the_post_thumbnail(); ?>
						</div>
						<div class="mini-blog-content col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<p>
								<?= crop_string(get_the_content(), 300, '...'); ?>
							</p>
							<div class="ref">
								<a href="<?= get_permalink(); ?>" title="<?php the_title(); ?>">Читать далее</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php endwhile; ?>

		</div>
	</div>
</section>
<?php get_footer(); ?>