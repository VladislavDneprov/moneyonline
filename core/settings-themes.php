<?php
add_action('admin_menu', 'register_my_custom_submenu_page');

function register_my_custom_submenu_page() {
	add_menu_page( 'Настройки темы', 'Настройки темы', 'edit_published_posts', 'my_theme_options', 'options_print' );
	add_submenu_page( 'my_theme_options', 'Камбекер', 'Камбекер', 'edit_published_posts', 'kambeker', 'kambeker_print' );
	add_submenu_page( 'my_theme_options', 'Фильтры', 'Фильтры', 'edit_published_posts', 'filters', 'filter_print' );
	
}


function options_print()
{
	?> <div class="wrap">
		<h2><?php echo get_admin_page_title() ?></h2>

		<form action="options.php" method="POST">
			<?php
				settings_fields( 'main-setting' );     // скрытые защитные поля
				do_settings_sections( 'Настройки темы' ); // секции с настройками (опциями). У нас она всего одна 'section_id'
				submit_button();
			?>
		</form>
	</div>
	<?php
}
function kambeker_print()
{
?> <div class="wrap">
		<h2><?php echo get_admin_page_title() ?></h2>

		<form action="options.php" method="POST">
			<?php
				settings_fields( 'kambeker-setting' );     // скрытые защитные поля
				do_settings_sections( 'Камбекер' ); // секции с настройками (опциями). У нас она всего одна 'section_id'
				submit_button();
			?>
		</form>
	</div>
	<?php
}

function filter_print()
{
	?><div class="wrap">
		<h2><?php echo get_admin_page_title() ?></h2>

		<form action="options.php" method="POST">
			<?php
				settings_fields( 'filter-setting' );     // скрытые защитные поля
				do_settings_sections( 'Фильтры' ); // секции с настройками (опциями). У нас она всего одна 'section_id'
				submit_button();
			?>
		</form>
	</div>
	<?php
}

/*
Регистрация главных настроек
*/
add_action('admin_init', 'main_setting');
function main_setting(){
	register_setting('main-setting', 'main_option'); //регистрация главных настроек
	add_settings_section('section_info', 'Главные настройки', '', 'Настройки темы'); //добавление секции для главных настроек
	add_settings_section('section_link', 'Настройки ссылок', '', 'Настройки темы'); 
	add_settings_field('phone','Телефон', 'fill_phone','Настройки темы', 'section_info');
	add_settings_field('email','Адрес эл. почты', 'fill_email','Настройки темы', 'section_info');
	$i = 1;
	foreach (array('С понедельника по пятницу','Суббота', 'Воскресенье') as $value)
	{
	add_settings_field('from_'.$i, $value, 'fill_info', 'Настройки темы', 'section_info',array('id' => $i, 'day' => $value)); //добавление опции к главным настройкам комбекера
	$i++;
	}
	foreach (array('Linkedin','Twitter','Facebook') as $value) {
	 	add_settings_field($value, $value, 'fill_link', 'Настройки темы', 'section_link',array('type' => $value));
	 } 
}
function fill_link($arg){
	$name = $arg['type'];
	$val = get_option('main_option');
	$val = $val[$name];
	?><label>
		<input type="text" name="main_option[<?=$name?>]" value="<?php echo esc_attr( $val ) ?>" />
		<p class="description" id="tagline-description">Ссылка на <?=$name;?>.</p>
	</label>
	<?php
}
function fill_phone(){
	$val = get_option('main_option');
	$val = $val['phone'];
	?>
	<label>
		<input type="text" name="main_option[phone]" value="<?php echo esc_attr( $val ) ?>" />
		<p class="description" id="tagline-description">Введите номер телефона.</p>
	</label>
	<?php

}
function fill_email(){
	$val = get_option('main_option');
	$val = $val['email'];
	?>
	<label>
		<input type="text" name="main_option[email]" value="<?php echo esc_attr( $val ) ?>" />
		<p class="description" id="tagline-description">Введите имейл.</p>
	</label>
	<?php

}
function fill_info($arg){
	$id = $arg['id'];
	$day = $arg['day'];
	switch ($day) {
		case 'С понедельника по пятницу':{
		$val = get_option('main_option');
		$val_from = $val['from_'.$id];
		$val_to = $val['to_'.$id];
		?>
		<label>
			<input type="time" name="main_option[from_<?=$id?>]" value="<?php echo esc_attr( $val_from ) ?>" />
			<input type="time" name="main_option[to_<?=$id?>]" value="<?php echo esc_attr( $val_to ) ?>" />
		</label>
		<?php } 
			break;
		
		case 'Суббота':{
		$val = get_option('main_option');
		$val_from = $val['from_'.$id];
		$val_to = $val['to_'.$id];
		$val_check = $val['check_'.$id];
			?>
		<label> 
			<input type="time" name="main_option[from_<?=$id?>]" value="<?php echo esc_attr( $val_from ) ?>" />
			<input type="time" name="main_option[to_<?=$id?>]" value="<?php echo esc_attr( $val_to ) ?>" />
			<input type="checkbox" name="main_option[check_<?=$id?>]" value="1" <?php checked( 1, $val_check ) ?> /> выходной
		</label>
		<?php }
			break;
		case 'Воскресенье':{
		$val = get_option('main_option');
		$val_from = $val['from_'.$id];
		$val_to = $val['to_'.$id];
		$val_check = $val['check_'.$id];
			?>
		<label>
			<input type="time" name="main_option[from_<?=$id?>]" value="<?php echo esc_attr( $val_from ) ?>" />
			<input type="time" name="main_option[to_<?=$id?>]" value="<?php echo esc_attr( $val_to ) ?>" />
			<input type="checkbox" name="main_option[check_<?=$id?>]" value="1" <?php checked( 1, $val_check ) ?> /> выходной
		</label>
		<?php } break;
	
	
}
}

/*
Регистрация настроек для комбекера
*/
add_action('admin_init', 'kambeker_setting');
function kambeker_setting(){
	register_setting('kambeker-setting', 'kambeker_option'); //регистрация настроек комбекера
	add_settings_section('section_id', 'Настройки камбекера', '', 'Камбекер'); //добавление секции для настроек комбекер

	add_settings_field('persen', 'Процент', 'fill_persent', 'Камбекер', 'section_id' ); //добавление опции к настройкам комбекера
	add_settings_field('list_company', 'Список компаний', 'fill_list_company', 'Камбекер', 'section_id' ); //добавление опции к настройкам комбекера
	add_settings_field('description', 'Описание', 'fill_description', 'Камбекер', 'section_id' ); //добавление опции к настройкам комбекера  
}
/*
Добавление опции "проценты" для комбекера
*/
function fill_persent(){
	$val = get_option('kambeker_option');
	$val = $val['input'];
	?>
	<input type="text" name="kambeker_option[input]" value="<?php echo esc_attr( $val ) ?>" />
	<p class="description" id="tagline-description">Количество людей в процентах сооветующих компанию.</p>
	<?php
}

/*
Добавление опции "список компаний" для комбекера
*/
function fill_list_company(){
	$val = get_option('kambeker_option');
	$val_1 = $val['select_1'];
	$val_2 = $val['select_2'];
	?>
	<label><select name="kambeker_option[select_1]">
			<?php foreach (get_posts(array('numberposts' => -1, 'post_status' => 'publish', 'post_type' => 'company')) as $key => $value): ?>
				<option value="<?=$value->ID;?>" <?php selected($value->ID, $val_1);?>><?=$value->post_title;?></option>
			<?php endforeach; ?>
		</select>
		<p class="description" id="tagline-description">Список компаний дающих кредит.</p>
	</label>
	<label><select name="kambeker_option[select_2]">
			<?php foreach (get_posts(array('numberposts' => -1, 'post_status' => 'publish', 'post_type' => 'company')) as $key => $value): ?>
				<option value="<?=$value->ID;?>" <?php selected($value->ID, $val_2);?>><?=$value->post_title;?></option>
			<?php endforeach; ?>
		</select>
		<p class="description" id="tagline-description">Список компаний дающих кредит.</p>
	</label>
	<?php
}
/*
Добавление опции "описание" для комбекера
*/
function fill_description(){
	$val = get_option('kambeker_option');
	$val = $val['textarea'];
	?>
	<label>
		<?php wp_editor($val, 'wpeditor', array('textarea_name' => 'kambeker_option[textarea]') ); ?>
		<p class="description" id="tagline-description">Напишите описание для компаний дающих кредиты.</p>
	</label>

<?php }

add_action('admin_init', 'filter_setting');
/*
Регистрация настроек фильтров
*/
function filter_setting(){
	register_setting('filter-setting', 'filter_option');
	for($i = 1; $i <= 4; $i++)
	{
		add_settings_section('section_'.$i, 'Настройки фильтра - '.$i, '', 'Фильтры');
		add_settings_field('filter_'.$i, 'Название', 'fill_filter', 'Фильтры', 'section_'.$i, array('id' => $i));
		add_settings_field('description'.$i, 'Описание', 'fill_description_filter', 'Фильтры', 'section_'.$i, array('id' => $i));
		add_settings_field('h1_'.$i, 'h1', 'fill_filter_h1', 'Фильтры', 'section_'.$i, array('id' => $i));
	
	}
}
function fill_filter($arg){
	$id = $arg['id'];
	$val = get_option('filter_option');
	$val = $val['input'.$id];
	?>
	<label>
		<input type="text" name="filter_option[input<?=$id?>]" value="<?php echo esc_attr( $val ) ?>" />
		<p class="description" id="tagline-description">Название фильтра</p>
	</label>
	<?php
}
function fill_description_filter($arg){
	$id = $arg['id'];
	$val = get_option('filter_option');
	$val = $val['textarea'.$id];
	?>
	<label>
		<input type="textarea" name="filter_option[textarea<?=$id?>]" value="<?php echo esc_attr( $val ) ?>" />
		<p class="description" id="tagline-description">Описание фильтра</p>
	</label>
	<?php
}
function fill_filter_h1($arg){
	$id = $arg['id'];
	$val = get_option('filter_option');
	$val = $val['input_h1_'.$id];
	?>
	<label>
		<input type="text" name="filter_option[input_h1_<?=$id?>]" value="<?php echo esc_attr( $val ) ?>" />
		<p class="description" id="tagline-description">Заголовок фильтра</p>
	</label>
	<?php
}

	
