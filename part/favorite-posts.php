<?php
	$args = array(
		'numberposts'     => 2,
		'offset'          => 0,
		'orderby'         => 'post_date',
		'order'           => 'DESC',
		'include'         => '',
		'exclude'         => '',
		'meta_key'        => '',
		'meta_value'      => '',
		'post_type'       => 'post',
		'post_parent'     => '',
		'post_status'     => 'publish'
	);

	$posts = get_posts($args);

?>
<div class="wide-container">
	<div class="row">
		<?php foreach ($posts as $key => $post): ?>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="mini-blog-block">
				<div class="mini-blog-header">
					<span><?= $post->post_title; ?></span>
				</div>
				<div class="mini-blog-body row">
					<div class="mini-blog-img col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<img src="<?= get_template_directory_uri().'/img/mini-blog-img.png'; ?>">
					</div>
					<div class="mini-blog-content col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<p><?= crop_string($post->post_content, 350); ?></p>
						<div class="ref">
							<a href="<?= get_permalink($post->ID); ?>">Читать далее</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php endforeach; ?>
	</div>
</div>