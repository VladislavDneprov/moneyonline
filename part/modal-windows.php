<div class="modal fade" tabindex="-1" role="dialog" id="modal-win-advertisment">
		<div class="modal-dialog modal-win-advertisment" role="document">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<div class="modal-header">					
                    <img src="<?= get_template_directory_uri().'/img/modal-img.png';?>">
                    <p>
                        <span class="title">25%</span><br>
                        <span>пользователей выбрали эти компании вместо <br>
						"Швидко займ":</span>
                    </p>
				</div>			
				<div class="modal-body">
					<?php $kambeker = get_option('kambeker_option'); $list_kambeker = array($kambeker['select_1'], $kambeker['select_2'] );?>
					<?php foreach ($list_kambeker as $value):?>
					<div class="post-block row">
			
						<div class="post-img col-lg-5 col-md-5 col-sm-5 col-xs-12 center-sm">
							<?= get_the_post_thumbnail($value); ?>
						</div>
						<div class="post-content col-lg-5 col-md-5 col-sm-5 col-xs-12 text-center-sm">
							<ul>
								<li>
									<p>Рейтинг: <span class="data">2.2</span></p>
								</li>
								<li>
									<p>Cрок: <span class="data"><?= get_post_meta($value, 'time_borrow_start', 1); ?> - <?= get_post_meta($value, 'time_borrow_finish', 1); ?> дня</span> </p>
									
								</li>
								<li>
									<p>Сумма: <span class="data">до <?= get_post_meta($value, 'summ', 1); ?></span></p>
									
								</li>
								<li>
									<p>Ставка <span class="data"><?= get_post_meta($value, 'rate', 1); ?>%/день</span></p>
									
								</li>
								<li>
									<p>Рассмотрение <span class="data"><?= get_post_meta($value, 'time', 1); ?> часов</span></p>
									
								</li>
							</ul>
						</div>
						<a target="blank" href="<?= get_post_meta($value, 'url', 1); ?>" title="Перейти на сайт" class="label"></a>
						<div class="post-order col-lg-2 col-md-2 col-sm-2 hidden-xs">
							<a target="blank" href="<?= get_post_meta($value, 'url', 1); ?>" title="Перейти на сайт">
							</a>
						</div>
					</div>
				<?php endforeach; ?>
				</div>
				<div class="modal-footer">
					<p>Продолжить оформление с "Швидко займ"</p>
				</div>
				
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->