<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Money online</title>

        <?php wp_head(); ?>
    </head>
    
    <body>
	    <header id="top">
            <div class="wide-container"> 
                <div class="row">         
                    <a href="<?= get_home_url(); ?>" class="logo col-lg-6 col-md-7 col-sm-6 col-xs-12">
                        <img src="<?= get_template_directory_uri().'/img/logo.png';?>">
                    </a>
        	    	<!--<nav class="menu col-lg-6 col-md-5 col-sm-12 col-xs-12">
                        <ul>
                            <li>
                                <a href="#">Блог</a>
                            </li>
                            <li>
                                <a href="#">Займ</a>
                            </li>
                        </ul>      
                    </nav>-->
                    <?= wp_nav_menu(array('theme_location' => 'top', 'container_class' => 'menu col-lg-6 col-md-5 col-sm-6 col-xs-12','container' => 'nav')); ?>
                </div>  
            </div>
	    </header>
        <section class="main-img">
            <div class="wide-container">
                <div class="main">
                    <div class="left">
                        <img src="<?= get_template_directory_uri().'/img/title-img.png';?>">
                        <p>
                            Самое быстрое <br> решение по кредиту
                        </p>
                    </div>
                    <img src="<?= get_template_directory_uri().'/img/main-img.png';?>" class="right hidden-sm hidden-xs">
                    <a class="download" href="#"></a>
                </div>
            </div>                
        </section>