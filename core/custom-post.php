<?php

# company adding post #

add_filter("manage_edit-company_sortable_columns", "my_column_register_sortable" );
add_filter('manage_company_posts_columns', 'new_company_col');

add_action('add_meta_boxes', 'my_extra_fields', 1);
add_action('manage_company_posts_custom_column', 'get_item_id_pp', 10, 2);
add_action('save_post', 'update_fields_credit_company', 0);

register_post_type('company',
    array(
      	'labels' => array(
			'name'               => 'Кредитные компании',
			'singular_name'      => 'Кредитная компания',
			'add_new'            => 'Добавить кредитную компанию',
			'add_new_item'       => 'Добавление новой кредитный компании',
			'edit_item'          => 'Страница редактирования кредитной компании',
			'search_items'       => 'Найти кредитную компанию',
			'not_found'          => 'Кредитный компаний не найдено',
			'not_found_in_trash' => 'Кредитных компаний нет в корзине',
			'menu_name'          => 'Кредитные компании',
		),
      'public' 				=> true,
      'publicly_queryable' 	=> true,
      'has_archive' 		=> false,
      'menu_position'		=> 4,
      'menu_icon'           => 'dashicons-admin-multisite',
      'supports'            => array('title','editor', 'thumbnail', 'trackbacks', 'comments'),
    )
);

function new_company_col($defaults) {

	$columns = array(
		'cb'	 		=> '<input type="checkbox" />',
		'logo'			=> 'Логотип',
		'title'			=> 'Имя кредитной компании',
		'time'			=> 'Время на расмотрении',
		'summ' 			=> 'Сумма кредита',
		'date'			=>	'Date',
	);
	return $columns;
}
 
function get_item_id_pp($column, $post_id) {
    if($column == 'summ')
        echo  '<big><b>'.get_post_meta($post_id, 'summ', 1).' грн.</b></big>';

    if($column == 'time')
        echo  '<b>'.get_post_meta($post_id, 'time', 1).'</b> часов.';

    if($column == 'logo')
        echo  '<big><b>'.get_the_post_thumbnail($post_id, 'full', ['style' => 'width:80px; height: auto;']).'</b></big>';
}

function my_column_register_sortable( $columns )
{
	$columns['summ'] = 'summ';
	return $columns;
}

function my_extra_fields() {
	add_meta_box( 'extra_fields', 'Настройки кредитной компании', 'fields_credit_company', 'company', 'normal', 'high'  );
}

function fields_credit_company( $post ){
	?>

	<p>
		<label>Рейтинг</label>
		<select name="credit_company[starss]">
			<?php $sel_stars = get_post_meta($post->ID, 'starss', 1); ?>
			<option value="0" <?php selected( $sel_stars, '0' ); ?> >Нет рейтинга</option>
			<option value="1" <?php selected( $sel_stars, '1' ); ?> >1 звезда</option>
			<option value="2" <?php selected( $sel_stars, '2' ); ?> >2 звезды</option>
			<option value="3" <?php selected( $sel_stars, '3' ); ?> >3 звезды</option>
			<option value="4" <?php selected( $sel_stars, '4' ); ?> >4 звезды</option>
			<option value="5" <?php selected( $sel_stars, '5' ); ?> >5 звёзд</option>
		</select>
	</p>

	<p>
		<label>Время займа</label>
		<input type="number" name="credit_company[time_borrow_start]" min="1" placeholder="3" value="<?= get_post_meta($post->ID, 'time_borrow_start', 1); ?>" /> -
		<input type="number" name="credit_company[time_borrow_finish]" min="1" placeholder="4" value="<?= get_post_meta($post->ID, 'time_borrow_finish', 1); ?>" /> дня
	</p>
	
	<p>
		<label>Сумма кредита: </label>
		до <input type="number" name="credit_company[summ]" min="0" step="1" placeholder="5000 грн." value="<?= get_post_meta($post->ID, 'summ', 1); ?>" /> грн.

		<label>Ставка</label>
		<input type="number" name="credit_company[rate]" min="0" step="0.1"  placeholder="5%" value="<?= get_post_meta($post->ID, 'rate', 1); ?>" /> %
	</p>

	<p>
		<label>Время на расмотрение</label>
		<input type="time" name="credit_company[time]" value="<?= get_post_meta($post->ID, 'time', 1); ?>" />
	</p>
	
	<?php
			$pages = get_pages(array(
				'numberposts' 		=> -1,
				'post_status' 		=> 'publish',
				'parent'			=> get_page_by_path('getway')->ID,
				'sort_column'		=> 'menu_order'
			));
	?>
	<p>
		<label>Способ получения: </label>
			<?php foreach ($pages as $key => $page): ?>
				<?php $sel_get_way = get_post_meta($post->ID, 'get_way_'.($key + 1), 1); ?>
				<label><input type="checkbox" name="credit_company[get_way_<?= $key + 1 ?>]" value="<?= $key + 1; ?>" <?php checked($sel_get_way, $key + 1); ?> /><?= $page->post_title; ?></label>
			<?php endforeach; ?>
		</select>
	</p>

	<p>
		<label>Внешняя сылка</label>
		<input type="url" name="credit_company[url]" style="width:50%;" placeholder="http://credit-online.com" value="<?= get_post_meta($post->ID, 'url', 1); ?>" />
	</p>

	<p>
	<?php
			$pages = get_pages(array(
				'numberposts' 		=> -1,
				'post_status' 		=> 'publish',
				'parent'			=> get_page_by_path('country')->ID,
				'sort_column'		=> 'menu_order'
			));
	?>
		<label>Город</label>
		<select name="credit_company[country]">
			<?php $sel_stars = get_post_meta($post->ID, 'country', 1); ?>
			<?php foreach ($pages as $key => $page): ?>
				<option value="<?= $key + 1; ?>" <?php selected( $sel_stars, $key + 1 ); ?> ><?= $page->post_title; ?></option>
			<?php endforeach; ?>
		</select>
	</p>
	
	<?php
			$pages = get_pages(array(
				'numberposts' 		=> -1,
				'post_status' 		=> 'publish',
				'parent'			=> get_page_by_path('lessons')->ID,
				'sort_column'		=> 'menu_order'
			));
	?>
	<p>
		<label>Род занятий</label>
		<select name="credit_company[lessons]">
			<?php $sel_stars = get_post_meta($post->ID, 'lessons', 1); ?>
			<?php foreach ($pages as $key => $page): ?>
				<option value="<?= $key + 1; ?>" <?php selected( $sel_stars, $key + 1 ); ?> ><?= $page->post_title; ?></option>
			<?php endforeach; ?>
		</select>
	</p>
	
	<?php
			$pages = get_pages(array(
				'numberposts' 		=> -1,
				'post_status' 		=> 'publish',
				'parent'			=> get_page_by_path('typeget')->ID,
				'sort_column'		=> 'menu_order'
			));
	?>
	<p>
		<label>Виды займов</label>
		<select name="credit_company[type]">
			<?php $sel_stars = get_post_meta($post->ID, 'type', 1); ?>
			<?php foreach ($pages as $key => $page): ?>
				<option value="<?= $key + 1; ?>" <?php selected( $sel_stars, $key + 1 ); ?> ><?= $page->post_title; ?></option>
			<?php endforeach; ?>
		</select>
	</p>

	<p>Новая: <?php $mark_the_best = get_post_meta($post->ID, 'new', 1); ?>
		<label><input type="radio" name="credit_company[new]" value="0" <?php checked( $mark_the_best, '0' ); ?> checked /> нет</label>
		<label><input type="radio" name="credit_company[new]" value="1" <?php checked( $mark_the_best, '1' ); ?> /> да</label>
	</p>

	<p>
		<?php $mark_the_best = get_post_meta($post->ID, 'sale', 1); ?>
		<label> Акция: <input type="radio" name="credit_company[sale]" value="0" <?php checked( $mark_the_best, '0' ); ?> checked /> нет</label>
		<label><input type="radio" name="credit_company[sale]" value="1" <?php checked( $mark_the_best, '1' ); ?> /> да</label>
	</p>
	
	<p>
		<?php $mark_the_best = get_post_meta($post->ID, 'top', 1); ?>
		<label> Топ: <input type="radio" name="credit_company[top]" value="0" <?php checked( $mark_the_best, '0' ); ?> checked /> нет</label>
		<label><input type="radio" name="credit_company[top]" value="1" <?php checked( $mark_the_best, '1' ); ?> /> да</label>
	</p>

	<input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />

	<input name="save" type="submit" class="button button-primary button-large" value="Сохранить">

	<?php
}

function update_fields_credit_company($post_id ) {
	if ( !isset($_POST['extra_fields_nonce']) || !wp_verify_nonce($_POST['extra_fields_nonce'], __FILE__) ) return false;
	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE  ) return false;
	if ( !current_user_can('edit_post', $post_id) ) return false;

	if( !isset($_POST['credit_company']) ) return false;

	if(!isset($_POST['credit_company']['get_way_1']))
		delete_post_meta($post_id, 'get_way_1');
	if(!isset($_POST['credit_company']['get_way_2']))
		delete_post_meta($post_id, 'get_way_2');
	if(!isset($_POST['credit_company']['get_way_3']))
		delete_post_meta($post_id, 'get_way_3');

	foreach( $_POST['credit_company'] as $key=>$value ){
		if( empty($value) ){
			delete_post_meta($post_id, $key);
			continue;
		}

		update_post_meta($post_id, $key, $value);
	}
	return $post_id;
}