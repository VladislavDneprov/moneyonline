<?php

require_once( __DIR__.'/core/custom-post.php' );
require_once( __DIR__.'/core/breadcrumbs.php' );
require_once( __DIR__.'/core/settings-themes.php' );

/** add javascript **/
add_action('wp_enqueue_scripts', 'add_scripts');
function add_scripts() {
	
	/** lib **/
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'bootstrap', 		get_template_directory_uri().'/js/lib/bootstrap.min.js', array('jquery'), '', true );
	wp_enqueue_script( 'slick', 			get_template_directory_uri().'/js/lib/slick/slick.min.js', array('jquery'), '', true );
	wp_enqueue_script( 'masked', 			get_template_directory_uri().'/js/lib/jquey.inputmask.bundle.min.js', array('jquery'), '', true );

	/** custom **/
	wp_enqueue_script( 'main', 				get_template_directory_uri().'/js/main.js', array('jquery'), '', true );

}

/** add css **/
add_action('wp_print_styles', 'add_styles');
function add_styles() {

	/** lib **/
	wp_enqueue_style( 'bootstrap', 			get_template_directory_uri().'/css/lib/bootstrap.min.css' );
	wp_enqueue_style( 'slick', 				get_template_directory_uri().'/js/lib/slick/slick.css' );
	wp_enqueue_style( 'slick-theme', 		get_template_directory_uri().'/js/lib/slick/slick-theme.css' );

	/** fonts **/
	wp_enqueue_style( 'font-awesome', 		get_template_directory_uri().'/css/lib/font-awesome.min.css' );

	/** custom **/
	wp_enqueue_style( 'main', 				get_template_directory_uri().'/style.css' );
	wp_enqueue_style( 'global', 			get_template_directory_uri().'/css/global.css' );

	if (is_home())
		wp_enqueue_style( 'index', 			get_template_directory_uri().'/css/index.css' );

	if (is_page()) {
		wp_enqueue_style( 'index', 			get_template_directory_uri().'/css/index.css' );
		wp_enqueue_style( 'page', 			get_template_directory_uri().'/css/page.css' );
	}

	if (is_single()) {
		wp_enqueue_style( 'index', 			get_template_directory_uri().'/css/index.css' );
		wp_enqueue_style( 'single', 		get_template_directory_uri().'/css/single.css' );
	}

	if ( 'company' == get_post_type() ) {
		wp_enqueue_style( 'index', 			get_template_directory_uri().'/css/index.css' );
		wp_enqueue_style( 'single-company', get_template_directory_uri().'/css/single-company.css' );
	}
	
	if (is_category())
		wp_enqueue_style( 'category', 		get_template_directory_uri().'/css/category.css' );
}

/** registration nav-menu **/
register_nav_menus(array(
	'top' 			=> 'Главное меню',
	'footer_1' 		=> 'Низ сайта, колонка 1',
	'footer_2' 		=> 'Низ сайта, колонка 2',
	'footer_3' 		=> 'Низ сайта, колонка 3',
	'footer_4' 		=> 'Низ сайта, колонка 3',
	'footer_5' 		=> 'Низ сайта, колонка 3',
	'footer_6' 		=> 'Низ сайта, колонка 3',
));

add_theme_support('post-thumbnails'); # add thubnails

remove_filter('the_content', 'wpautop');

/** user functions **/
function crop_string($string, $count, $add_symbol = '')
{  
   return substr($string, 0, $count).((strlen($string) > $count) ? $add_symbol : '');
}