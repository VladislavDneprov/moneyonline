<?php get_header(); ?>

<section class="filters">
	<div class="wide-container">	
		<div class="row">
			<div class="filter-block">
				<button class="dropdown styled-select" data-toggle="dropdown"><?= get_page_by_path('country')->post_title; ?></button>
				<?php
					$pages = get_pages(array(
						'numberposts' 		=> -1,
						'post_status' 		=> 'publish',
						'parent'			=> get_page_by_path('country')->ID,
						'sort_column'		=> 'menu_order'
					));
				?>
				<ul class="dropdown-menu">
					<?php foreach ($pages as $key => $page): ?>
						<li>
							<form action="<?= get_permalink($page->ID); ?>" method="post">
								<input type="hidden" value="<?= $key+1 ?>" name="value" />
								<input type="hidden" value="get_way_<?= $key+1 ?>" name="key" />
								<input type="hidden" value="1" name="filter" />
								<button class="btn-filter" type="submit"><?= $page->post_title; ?></button>
							</form>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>	
			<div class="filter-block">
				<button class="dropdown styled-select" data-toggle="dropdown">Сортировка по:</button>
				<ul class="dropdown-menu">
					<li>
						<form action="<?= site_url('/'); ?>" method="post">
							<input type="hidden" value="rate" name="key" />
							<input type="hidden" value="1" name="sort" />
							<input type="hidden" value="2" name="filter" />
							<button class="btn-filter" type="submit">Ставке</button>
						</form>
						<form action="<?= site_url('/'); ?>" method="post">
							<input type="hidden" value="summ" name="key" />
							<input type="hidden" value="1" name="sort" />
							<input type="hidden" value="2" name="filter" />
							<button class="btn-filter" type="submit">Сумме</button>
						</form>
						<form action="<?= site_url('/'); ?>" method="post">
							<input type="hidden" value="time_borrow_finish" name="key" />
							<input type="hidden" value="1" name="sort" />
							<input type="hidden" value="2" name="filter" />
							<button class="btn-filter" type="submit">Сроку</button>
						</form>
						<form action="<?= site_url('/'); ?>" method="post">
							<input type="hidden" value="time" name="key" />
							<input type="hidden" value="1" name="sort" />
							<input type="hidden" value="2" name="filter" />
							<button class="btn-filter" type="submit">Времени</button>
						</form>
						<form action="<?= site_url('/'); ?>" method="post">
							<input type="hidden" value="starss" name="key" />
							<input type="hidden" value="1" name="sort" />
							<input type="hidden" value="2" name="filter" />
							<button class="btn-filter" type="submit">Рейтингу</button>
						</form>
					</li>
				</ul>
			</div>
			<div class="filter-block">
				<button class="dropdown styled-select" data-toggle="dropdown"><?= get_page_by_path('getway')->post_title; ?></button>
				<?php
					$pages = get_pages(array(
						'numberposts' 		=> -1,
						'post_status' 		=> 'publish',
						'parent'			=> get_page_by_path('getway')->ID,
						'sort_column'		=> 'menu_order'
					));
				?>
				<ul class="dropdown-menu">
					<?php foreach ($pages as $key => $page): ?>
						<li>
							<form action="<?= get_permalink($page->ID); ?>" method="post">
								<input type="hidden" value="<?= $key+1 ?>" name="value" />
								<input type="hidden" value="get_way_<?= $key+1 ?>" name="key" />
								<input type="hidden" value="1" name="filter" />
								<button class="btn-filter" type="submit"><?= $page->post_title; ?></button>
							</form>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
			<div class="filter-block">
				<button class="dropdown styled-select" data-toggle="dropdown"><?= get_page_by_path('lessons')->post_title; ?></button>
				<?php
					$pages = get_pages(array(
						'numberposts' 		=> -1,
						'post_status' 		=> 'publish',
						'parent'			=> get_page_by_path('lessons')->ID,
						'sort_column'		=> 'menu_order'
					));
				?>
				<ul class="dropdown-menu">
					<?php foreach ($pages as $key => $page): ?>
						<li>
							<form action="<?= get_permalink($page->ID); ?>" method="post">
								<input type="hidden" value="<?= $key+1 ?>" name="value" />
								<input type="hidden" value="get_way_<?= $key+1 ?>" name="key" />
								<input type="hidden" value="1" name="filter" />
								<button class="btn-filter" type="submit"><?= $page->post_title; ?></button>
							</form>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
			<div class="filter-block">
				<button class="dropdown styled-select" data-toggle="dropdown"><?= get_page_by_path('typeget')->post_title; ?></button>
				<?php
					$pages = get_pages(array(
						'numberposts' 		=> -1,
						'post_status' 		=> 'publish',
						'parent'			=> get_page_by_path('typeget')->ID,
						'sort_column'		=> 'menu_order'
					));
				?>
				<ul class="dropdown-menu">
					<?php foreach ($pages as $key => $page): ?>
						<li>
							<form action="<?= get_permalink($page->ID); ?>" method="post">
								<input type="hidden" value="<?= $key+1 ?>" name="value" />
								<input type="hidden" value="get_way_<?= $key+1 ?>" name="key" />
								<input type="hidden" value="1" name="filter" />
								<button class="btn-filter" type="submit"><?= $page->post_title; ?></button>
							</form>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="breadcrumbs col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center-sm">
				<?php get_template_part( 'part/breadcrumbs' ) ?>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center-sm">
				<?php if ( have_posts() && is_page() ) : ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<h1 style="color: #5ea8d4; margin-bottom:0;"><?php the_title(); ?></h1>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>

</section>

<section class="posts">	
		<?php get_template_part( 'part/list-companies' ) ?>			
</section>

<section class="info">
	<?php get_template_part( 'part/content-page' ) ?>	
</section>

<section class="mini-blog">
	<?php get_template_part( 'part/favorite-posts' ) ?>			
</section>

<?php get_footer(); ?>