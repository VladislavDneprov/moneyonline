jQuery(function($){

	function Menu(){
		var top = $(document).scrollTop();
		if (top < 800) {
			$("#toTop").removeClass('float');							
		}				
		else {
			$("#toTop").addClass('float');						
		}
	}

	$(document).ready(function(){

		$('#toTop').click(function(){
	        var el = $(this).attr('href');
	        $('body').animate({
	            scrollTop: $(el).offset().top}, 700);
	        return false; 
		});
		

		Menu();
			
		$(window).scroll(function() {
			Menu();
		});
		setTimeout(function(){
			$('#modal-win-advertisment').modal('show')
		}, 30000);	
	});
});