<?php
/**
 * Шаблон подвала (footer.php)
 * @package WordPress
 * @subpackage money-online
 */
 ?>
		 <footer>
		 	<div class="footer-top">
		 		<div class="wide-container">
		 			<div class="footer-content">
		 				<div class="row">
			 				<div class="ref-block col-lg-2 col-md-3 col-sm-4 col-xs-6">
			 					<span class="title">Блок ссылок 1</span>
			 					<?= wp_nav_menu(array('theme_location' => 'footer_1','container' => false)); ?>			
			 				</div>
			 				<div class="ref-block col-lg-2 col-md-3 col-sm-4 col-xs-6">
			 					<span class="title">Блок ссылок 2</span>
			 					<?= wp_nav_menu(array('theme_location' => 'footer_2','container' => false)); ?>
			 				</div>
			 				<div class="ref-block col-lg-2 col-md-3 col-sm-4 col-xs-6">
			 					<span class="title">Блок ссылок 3</span>
			 					<?= wp_nav_menu(array('theme_location' => 'footer_3','container' => false)); ?>
			 				</div>
			 				<div class="ref-block col-lg-2 col-md-3 col-sm-4 col-xs-6">
			 					<span class="title">Блок ссылок 4</span>
			 					<?= wp_nav_menu(array('theme_location' => 'footer_4','container' => false)); ?>
			 				</div>
			 				<div class="ref-block col-lg-2 col-md-3 col-sm-4 col-xs-6">
			 					<span class="title">Блок ссылок 5</span>
			 					<?= wp_nav_menu(array('theme_location' => 'footer_5','container' => false)); ?>
			 				</div>
			 				<div class="ref-block col-lg-2 col-md-3 col-sm-4 col-xs-6">
			 					<span class="title">Блок ссылок 6</span>
			 					<?= wp_nav_menu(array('theme_location' => 'footer_6','container' => false)); ?>
			 				</div>
			 			</div>
		 			</div> 			
		 		</div>
		 	</div>
		 	<!--<div class="footer-bot"></div>-->
		 	<!--<button type="button" class="btn" data-toggle="modal" data-target="#modal-win-advertisment">
			  Launch demo modal
			</button>-->
			<?php get_template_part( 'part/modal-windows' ); ?>
		 	<a id="toTop" href="#top" class="hidden-xs"><i class="fa fa-arrow-up"></i></a>
		 </footer>
		 <?php wp_footer(); // необходимо для работы плагинов и функционала  ?>
	</body>
</html>